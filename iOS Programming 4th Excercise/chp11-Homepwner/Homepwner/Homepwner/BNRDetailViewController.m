//
//  BNRDetailViewController.m
//  Homepwner
//
//  Created by lxl on 15/6/14.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRDetailViewController.h"
#import "BNRItem.h"
#import "BNRImageStore.h"
//此处的协议除了要写上UIImagePickerControllerDelegate之外还要写上UINavigationControllerDelegate，是因为前者是后者的子类，所以该delegate也要遵守后者的协议
@interface BNRDetailViewController ()  <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *serialNumberField;
@property (weak, nonatomic) IBOutlet UITextField *valueField;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@end

@implementation BNRDetailViewController
#pragma mark - property
-(BNRItem *)itemToShow
{
    self.navigationItem.title = _itemToShow.itemName;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(saveData)];
    return _itemToShow;
}

#pragma mark - view life cycle

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    NSLog(@"%@",self.itemToShow.description);
    self.nameField.text = self.itemToShow.itemName;
    self.serialNumberField.text = self.itemToShow.serialName;
    self.valueField.text = [NSString stringWithFormat:@"%d",self.itemToShow.valueInDollars];
    self.valueField.keyboardType =  UIKeyboardTypeNumberPad;
    //处理nsdate对象转换为一定风格的string表示
    static NSDateFormatter *formatter = nil;
    if(!formatter){
        formatter = [[NSDateFormatter alloc]init];
        formatter.dateStyle = NSDateFormatterMediumStyle;
        formatter.timeStyle = NSDateFormatterNoStyle;
    }
    self.dateLabel.text = [formatter stringFromDate:self.itemToShow.dateCreated];
    //再次载入detail时，显示图片
    self.imageView.image = [[BNRImageStore shareStore] imageForKey:self.itemToShow.itemKey];
    
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self saveData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma  mark - action

-(void) saveData
{
    //取消当前的第一响应对象
    [self.view endEditing:YES];
    self.itemToShow.itemName = self.nameField.text;
    self.itemToShow.serialName = self.serialNumberField.text;
    self.itemToShow.valueInDollars = [self.valueField.text intValue];//强制将字符串转换为整型数据
}


//为拍照按钮做响应事件
- (IBAction)takePicture:(id)sender {
    //初始化UIImagePickerController的控制器
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    //imagePicker的sourcetype有三种类型，首先要测试是否支持，支持的话 再设置
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }else{
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    //imagePicker的delegate,用来处理选择照片或者拍照之后的事件
    imagePicker.delegate = self;
    //设置imagePicker允许编辑
    imagePicker.allowsEditing = YES;
    //以模态（modal）的形式显示UIImagePickerController对象
    [self presentViewController:imagePicker animated:YES completion:nil];
}


//选择到一张图片之后 同步数据源，保存到字典中，imagePicker的一个delegate方法
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *image = info[UIImagePickerControllerEditedImage];//需要先对imagePicker的allowsEditing属性设置为YES
    [[BNRImageStore shareStore] setImage:image forKey:self.itemToShow.itemKey];
    self.imageView.image = image;
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}
//点击非编辑区域 使键盘消失，由于UIControl是UIView的子类，这里需要将xib文件的自定义类由UIView设置为UIControl，再对整个顶层View设置IBAction来达到endEditting的效果。
- (IBAction)backgroundTapped:(id)sender {
    [self.view endEditing:YES];
}
//对已选的图片进行清除
- (IBAction)clearImage:(UIBarButtonItem *)sender {
    self.imageView.image = nil;
    [[BNRImageStore shareStore] deleteImageForKey:self.itemToShow.itemKey];

}


@end
