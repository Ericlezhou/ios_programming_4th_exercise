//
//  BNRImageStore.m
//  Homepwner
//
//  Created by lxl on 15/6/14.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRImageStore.h"

@interface BNRImageStore()

@property (nonatomic, strong) NSMutableDictionary *dictionary;

@end


@implementation BNRImageStore
//禁止使用init方法
-(instancetype)init
{
    @throw [NSException exceptionWithName:@"Singleton" reason:@"Use + [BNRImageStore shareStore]" userInfo:nil];
}
//私有init方法，init的同时初始化dictionary
-(instancetype)initPrivate
{
    self = [super init];
    if (self) {
        _dictionary = [[NSMutableDictionary alloc]init];
    }
    return self;
}
//类方法 产生一个单例
+(instancetype)shareStore
{
    static BNRImageStore *shareStore = nil;
    if (!shareStore) {
        shareStore = [[self alloc] initPrivate];
    }
    return shareStore;
}


//针对一个key设置一个图片，即填充字典
-(void)setImage:(UIImage *)image forKey:(NSString *)key
{
    [_dictionary setObject:image forKey:key];
//    _dictionary[key] = image;
}
//针对一个key从字典中获取value，即uiimage
-(UIImage *)imageForKey:(NSString *)key
{
    return [_dictionary objectForKey:key];
//    return  _dictionary[key];
}
//删除key对应的image
-(void)deleteImageForKey:(NSString *)key
{
    //attention!
    if (!key) {
        return;
    }
    [_dictionary removeObjectForKey:key];
}

@end
