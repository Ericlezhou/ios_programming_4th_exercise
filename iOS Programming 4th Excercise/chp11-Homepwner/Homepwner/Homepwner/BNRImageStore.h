//
//  BNRImageStore.h
//  Homepwner
//
//  Created by lxl on 15/6/14.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIImage;
@interface BNRImageStore : NSObject
+ (instancetype) shareStore;

- (void) setImage:(UIImage *)image forKey: (NSString *)key;
- (UIImage *)imageForKey:(NSString *)key;
- (void)deleteImageForKey:(NSString *)key;
@end
