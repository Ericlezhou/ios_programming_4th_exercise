//
//  BNRItem.h
//  RandomItems
//
//  Created by lxl on 15/6/7.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNRItem : NSObject

@property (nonatomic, copy)NSString *itemName;
@property (nonatomic, copy)NSString *serialName;
@property (nonatomic)int valueInDollars;
@property (nonatomic, strong,readonly)NSDate *dateCreated;
@property (nonatomic, copy)NSString *itemKey;

+ (instancetype) randomItem;

- (instancetype) initWithItemName: (NSString *)name
                   valueInDollars: (int)value
                     serialNumber: (NSString *)sNumber;
- (instancetype) initWithItemName: (NSString *)name;

- (instancetype) initWithItemName:(NSString *)name serialNumber:(NSString *) sNumber;
@end
