//
//  BNRHypnosisView.m
//  Hypnosister
//
//  Created by lxl on 15/6/8.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRHypnosisView.h"
@interface BNRHypnosisView()

@property (nonatomic, strong) UIColor *circleColor;//圆环变化的颜色

@end



@implementation BNRHypnosisView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _circleColor = [UIColor redColor];
        
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect
{
    CGRect bounds = self.bounds;
    
    // Figure out the center of the bounds rectangle
    CGPoint center;
    center.x = bounds.origin.x + bounds.size.width / 2.0;
    center.y = bounds.origin.y + bounds.size.height / 2.0;
    
    // The largest circle will circumstribe the view
    float maxRadius = hypot(bounds.size.width, bounds.size.height) / 2.0;
    
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    for (float currentRadius = maxRadius; currentRadius > 0; currentRadius -= 20) {
        [path moveToPoint:CGPointMake(center.x + currentRadius, center.y)];
        
        [path addArcWithCenter:center
                        radius:currentRadius
                    startAngle:0.0
                      endAngle:M_PI*2
                     clockwise:YES];
    }
    
    // Configure line width to 10 points
    path.lineWidth = 10;
    
    [_circleColor setStroke];
    
    // Draw the line!
    [path stroke];
    path = nil;
    CGFloat bgWidth = rect.size.width;
    CGFloat bgHeight = rect.size.height;
    CGPoint subOrigin;
    subOrigin.y = rect.origin.y + bgHeight - 88;
    subOrigin.x = rect.origin.x +(bgWidth)/6.0;
    CGRect subRect = CGRectMake(subOrigin.x, subOrigin.y,bgWidth/3*2, 44);
    UISegmentedControl *segCtrl = [[UISegmentedControl alloc] initWithFrame:subRect];
    
    [segCtrl insertSegmentWithTitle:@"red" atIndex:0 animated:NO];
    [segCtrl insertSegmentWithTitle:@"green" atIndex:1 animated:NO];
    [segCtrl insertSegmentWithTitle:@"blue" atIndex:2 animated:NO
     ];
    [segCtrl addTarget:self action:@selector(selected:) forControlEvents:UIControlEventValueChanged];
    [segCtrl setMomentary:YES];
    [self addSubview:segCtrl];
    segCtrl = nil;
    
    
}
- (void) selected:(UISegmentedControl *)sender
{
    switch (sender.selectedSegmentIndex) {
        case 0:
            self.circleColor = [UIColor redColor];
            [self setNeedsDisplay];
            break;
            
        case 1:
            self.circleColor = [UIColor greenColor];
            [self setNeedsDisplay];
            break;
        case 2:
            self.circleColor = [UIColor blueColor];
            [self setNeedsDisplay];
            break;
            
        default:
            break;
    }
    
}

//处理触摸事件，改变圆环颜色
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    float red = (arc4random()%100)/100.0;
    float green = (arc4random()%100)/100.0;
    float blue = (arc4random()%100)/100.0;
    _circleColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    [self setNeedsDisplay];
    
}


@end
