//
//  BNRHypnosisViewController.m
//  HypnoNerd
//
//  Created by lxl on 15/6/10.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRHypnosisViewController.h"
#import "BNRHypnosisView.h"
@interface BNRHypnosisViewController()

@end

@implementation BNRHypnosisViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        self.tabBarItem.title = @"Hypnotize";
        UIImage *i = [UIImage imageNamed:@"Hypno.png"];
        self.tabBarItem.image = i;
    }
    return self;
    
}


- (void)loadView
{
    //创建一个BNRHypnosisview
    BNRHypnosisView *backgroundView = [[BNRHypnosisView alloc] init];
    //将创建的BNRHypnosisView对象赋值给视图控制器的view属性
    self.view = backgroundView;
    

    
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"page 1 did load!");
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"page 1 will appear!");
}

@end
