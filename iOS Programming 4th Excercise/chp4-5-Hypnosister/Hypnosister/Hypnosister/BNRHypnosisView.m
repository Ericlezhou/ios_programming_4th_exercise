//
//  BNRHypnosisView.m
//  Hypnosister
//
//  Created by lxl on 15/6/8.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRHypnosisView.h"
@interface BNRHypnosisView()

@property (nonatomic, strong) UIColor *circleColor;//圆环变化的颜色

@end



@implementation BNRHypnosisView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        _circleColor = [UIColor lightGrayColor];
        
        
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect
{
    CGRect bounds = self.bounds;
    
    // Figure out the center of the bounds rectangle
    CGPoint center;
    center.x = bounds.origin.x + bounds.size.width / 2.0;
    center.y = bounds.origin.y + bounds.size.height / 2.0;
    
    // The largest circle will circumstribe the view
    float maxRadius = hypot(bounds.size.width, bounds.size.height) / 2.0;
    
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    for (float currentRadius = maxRadius; currentRadius > 0; currentRadius -= 20) {
        [path moveToPoint:CGPointMake(center.x + currentRadius, center.y)];
        
        [path addArcWithCenter:center
                        radius:currentRadius
                    startAngle:0.0
                      endAngle:M_PI*2
                     clockwise:YES];
    }
    
    // Configure line width to 10 points
    path.lineWidth = 10;
    
    [_circleColor setStroke];
    
    // Draw the line!
    [path stroke];
    path = nil;
    
    /**********构造一个内中央矩形区域*********/
    //先找到内中央2/3的新的rect
    CGFloat sub_width = rect.size.width*2/3;
    CGFloat sub_height = rect.size.height*2/3;
    CGPoint sub_zero_point = CGPointMake(rect.origin.x+rect.size.width/2-sub_width/2, rect.origin.y+rect.size.height/2-sub_height/2);
    
    /*********绘制线性渐变的效果********/
    //CGColorSpaceRef指定一种颜色空间，这里指定的是RGB, 此处不能是indexed或者pattern的
    UIBezierPath *mypath = [[UIBezierPath alloc] init];
    //在整个rect区域的内中央2/3处放置一个圆锥形状的渐变效果
    CGPoint startPoint = CGPointMake(sub_zero_point.x+sub_width/2, sub_zero_point.y);
    CGPoint midPoint = CGPointMake( sub_zero_point.x, sub_zero_point.y+sub_height);
    CGPoint endPoint = CGPointMake(sub_zero_point.x+sub_width, sub_zero_point.y+sub_height);
    [mypath moveToPoint:startPoint];
    [mypath addLineToPoint:midPoint];
    [mypath addLineToPoint:endPoint];
    [mypath addLineToPoint:startPoint];
    //获取上下文环境-CGContextRef，它是一个对上下文结构的引用，即指向上下文对象的一个指针
    //CGContextRef类同于CGMutablePathRef和CGColorRef
    CGContextRef currentContext = UIGraphicsGetCurrentContext();//获取当前上下文环境
    
    CGContextSaveGState(currentContext);//保存当前上下文环境
    [mypath addClip];//剪切path包围的区域，并在该区域进行绘图处理
    //初始化渐变特效的一些属性
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    CGFloat components[8] = {1.0, 1.0, 0.0, 1.0,//起始颜色是黄色
        0.0,1.0,0.0,1.0};//终止颜色是绿色
    CGFloat locations[2] = {0.0, 1.0};
    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorspace, components, locations, 2);
    //对上下文环境设置渐变特效
    CGContextDrawLinearGradient(currentContext, gradient, startPoint, endPoint, 0);
    CGGradientRelease(gradient);//对于创建对象时 如果出现了copy或者create单词的方法，使用完之后一定要记得释放
    CGColorSpaceRelease(colorspace);//同上
    CGContextRestoreGState(currentContext);//恢复保存的上下文环境
    
    /**********绘制带有阴影效果的logo图片********/
    //保存之前的上下文环境
    CGContextSaveGState(currentContext);
    //对上下文环境设置阴影特效
    CGContextSetShadow(currentContext, CGSizeMake(4, 7), 3);
    //下面绘制的图像会带有阴影效果
    //在整个rect区域的内中央2/3处放置一个带有阴影的图片
    CGRect logoRect = CGRectMake(sub_zero_point.x, sub_zero_point.y, sub_width, sub_height);
    UIImage *logo = [UIImage imageNamed:@"logo.png"];
    [logo drawInRect:logoRect];
    //恢复保存的上下文环境，恢复没有阴影效果的状态，停止绘画阴影效果图像
    CGContextRestoreGState(currentContext);
    
}
//处理触摸事件，改变圆环颜色
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    float red = (arc4random()%100)/100.0;
    float green = (arc4random()%100)/100.0;
    float blue = (arc4random()%100)/100.0;
    _circleColor = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    [self setNeedsDisplay];
    
}


@end
