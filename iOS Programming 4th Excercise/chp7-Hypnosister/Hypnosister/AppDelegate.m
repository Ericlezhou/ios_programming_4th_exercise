//
//  AppDelegate.m
//  Hypnosister
//
//  Created by lxl on 15/6/8.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "AppDelegate.h"
#import "BNRHypnosisView.h"
@interface AppDelegate () <UIScrollViewDelegate>

@property (nonatomic,strong) BNRHypnosisView *hypnosisview;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    CGRect screenRect = self.window.bounds;
    CGRect bigRect = CGRectMake(screenRect.origin.x, screenRect.origin.y, screenRect.size.width*2, screenRect.size.height*2);

    UIScrollView *scrollview = [[UIScrollView alloc] initWithFrame:screenRect];
    
    [scrollview setPagingEnabled:NO];
    scrollview.minimumZoomScale = 1.0;
    scrollview.maximumZoomScale = 3.0;
    scrollview.delegate = self;
    scrollview.contentSize = bigRect.size;

    [self.window addSubview:scrollview];
    
    self.hypnosisview = [[BNRHypnosisView alloc] initWithFrame:screenRect];
    self.hypnosisview.backgroundColor = [UIColor redColor];
    
    [scrollview addSubview:self.hypnosisview];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return  self.hypnosisview;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
