//
//  BNRReminderViewController.m
//  HypnoNerd
//
//  Created by lxl on 15/6/10.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRReminderViewController.h"

@interface BNRReminderViewController()

@property (nonatomic, weak) IBOutlet UIDatePicker *datePicker;

@end

@implementation BNRReminderViewController

- (instancetype) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tabBarItem.title = @"Reminder";
        UIImage *i = [UIImage imageNamed:@"Time.png"];
        self.tabBarItem.image = i;
    }
    return self;
}


- (IBAction)addReminder:(UIButton *)sender
{
    NSDate *date = self.datePicker.date;
    UILocalNotification *note = [[UILocalNotification alloc] init];
    note.alertBody = @"Hypnotize";
    note.fireDate = date;
    //添加下面的code来注册本地通知的setting
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *noteSetting =[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:noteSetting];
    }
    
    [[UIApplication sharedApplication] scheduleLocalNotification:note];
}

-(void)viewDidLoad
{
    [super  viewDidLoad];
    NSLog(@"page 2 did load!");
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"page 2 will appear!");
    self.datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:60];
}

@end
