//
//  BNRHypnosisViewController.m
//  HypnoNerd
//
//  Created by lxl on 15/6/10.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRHypnosisViewController.h"
#import "BNRHypnosisView.h"
@interface BNRHypnosisViewController () <UITextFieldDelegate>

@end


@implementation BNRHypnosisViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        self.tabBarItem.title = @"Hypnotize";
        UIImage *i = [UIImage imageNamed:@"Hypno.png"];
        self.tabBarItem.image = i;
    }
    return self;
}

- (void)loadView
{
    CGRect frame = [[UIScreen mainScreen] bounds];
    
    //创建一个BNRHypnosisview
    BNRHypnosisView *backgroundView = [[BNRHypnosisView alloc] initWithFrame:frame];
    NSLog(@"width=%f,height=%f",backgroundView.bounds.size.width,backgroundView.bounds.size.height);
    CGRect textFileRect = CGRectMake(40, 70, 240, 30);
    UITextField *textField = [[UITextField alloc] initWithFrame:textFileRect];
    //设置textfield的边框样式
    [textField setBorderStyle:UITextBorderStyleRoundedRect];
    textField.placeholder = @"Hypnotize me";//hint
    textField.returnKeyType = UIReturnKeyDone;//回车键显示为完成，即为Done
    
    textField.delegate = self;//将textfiled的委托属性设置为BNRHypnosisViewController自身
    
    
    [backgroundView addSubview:textField];
     //将焦点对住textfiled
//    [textField becomeFirstResponder];
    
//    将创建的BNRHypnosisView对象赋值给视图控制器的view属性
    self.view = backgroundView;
}

- (void) drawHypnoticMessage:(NSString *)message
{
    for (int i = 0; i < 20; i++) {
        UILabel *messageLabel = [[UILabel alloc] init];
        messageLabel.backgroundColor = [UIColor clearColor];
        messageLabel.textColor = [UIColor whiteColor];
        messageLabel.text = message;
        
        [messageLabel sizeToFit];
        
        int width = self.view.bounds.size.width - messageLabel.bounds.size.width;
        int x = arc4random()%width;
        int height = self.view.bounds.size.height - messageLabel.bounds.size.height;
        int y = arc4random()%height;
        CGRect frame = messageLabel.frame;
        frame.origin = CGPointMake(x, y);
        messageLabel.frame = frame;
        [self.view addSubview:messageLabel];
        
        UIInterpolatingMotionEffect *motionEffect;
        motionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
        motionEffect.minimumRelativeValue = @(-25);
        motionEffect.maximumRelativeValue = @(25);
        [messageLabel addMotionEffect:motionEffect];
        
        motionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
        motionEffect.minimumRelativeValue = @(-25);
        motionEffect.maximumRelativeValue = @(25);
        [messageLabel addMotionEffect:motionEffect];
    }
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self drawHypnoticMessage:textField.text];
    textField.text = @"";
    [textField resignFirstResponder];
    return YES;
}




-(void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"page 1 did load!");
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"page 1 will appear!");
}

@end
