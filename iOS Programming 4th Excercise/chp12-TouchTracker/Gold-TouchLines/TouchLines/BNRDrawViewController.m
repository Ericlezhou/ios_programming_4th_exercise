//
//  BNRDrawViewController.m
//  TouchTracker
//
//  Created by lxl on 15/6/15.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRDrawViewController.h"
#import "BNRDrawView.h"

@interface BNRDrawViewController ()

@property (nonatomic,strong) BNRDrawView *panelView;
@end

@implementation BNRDrawViewController

#pragma mark - property
- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self){
        CGRect panelRect = CGRectMake(0,20, 320, 480);
        _panelView = [[BNRDrawView alloc] initWithFrame:panelRect];
        [self.view addSubview:_panelView];
    }
    return self;
}

#pragma mark - view life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - actions

- (IBAction)clearLines:(id)sender {
    [_panelView clearData];
}

@end
