//
//  BNRDrawView.m
//  TouchTracker
//
//  Created by lxl on 15/6/15.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRDrawView.h"
#import  "BNRLine.h"

@interface BNRDrawView()

@property (nonatomic,strong) BNRLine *currentLine;
@property (nonatomic,strong) BNRLine *currentCircle;
@property (nonatomic,strong) NSMutableDictionary *cir;
@property (nonatomic,strong) NSMutableArray *finishedLines;
@property (nonatomic,strong) NSMutableArray *finishedCircles;
@property (nonatomic,strong) NSString *savePath;//保存历史记录的文件路径
@end

@implementation BNRDrawView

 - (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.finishedLines = [[NSMutableArray alloc] init];
        self.finishedCircles = [[NSMutableArray alloc] init];
        self.cir = [[NSMutableDictionary alloc] init];
        self.backgroundColor = [UIColor grayColor];
        self.multipleTouchEnabled = YES;
        if([[NSKeyedUnarchiver unarchiveObjectWithFile:self.savePath] count] > 0)
        {
            self.finishedLines = [NSKeyedUnarchiver unarchiveObjectWithFile:self.savePath];
        }
    }
    return self;
}


- (NSString*)savePath{
    //获得Documents目录的文件路径
    NSArray *array =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [array firstObject];
    return [path stringByAppendingPathComponent:@"f.archive"];
}


- (void) strokeLine : (BNRLine *)line{
    UIBezierPath *path = [[UIBezierPath alloc]init];
    path.lineWidth = 5;
    path.lineCapStyle = kCGLineCapRound;
    [path moveToPoint:line.begin];
    [path addLineToPoint:line.end];
    [path stroke];
}

- (void) strokeCircle : (BNRLine *)line{
    if (line.begin.x == line.end.x && line.begin.y == line.end.y) {
        [self strokeLine:line];
        return;
    }
    UIBezierPath *path = [[UIBezierPath alloc] init];
    path.lineWidth= 5;
    CGPoint point = CGPointMake((line.begin.x + line.end.x)/2, (line.begin.y + line.end.y)/2);
    CGFloat radius = 0.5*sqrt(pow((line.begin.x - line.end.x),2)+pow((line.begin.y - line.end.y), 2));
    CGFloat startAngle;
    if (line.begin.x == line.end.x && line.begin.y < line.end.y) {
        startAngle = M_PI/2;
    }else if(line.begin.x == line.end.x && line.begin.y > line.end.y){
        startAngle = -M_PI/2;
    }else
    {
        if (line.begin.x <= line.end.x) {
            
            startAngle = atan((line.end.y -line.begin.y)/(line.end.x - line.begin.x ));
        }else{
            startAngle = atan((line.end.y -line.begin.y)/(line.end.x - line.begin.x ))+M_PI;
        }
    }
    [path moveToPoint:line.end];
    [path addArcWithCenter:point radius:radius startAngle:startAngle endAngle:2*M_PI+startAngle clockwise:YES];
    [path stroke];
}

//重写UIView的drawRect方法
- (void)drawRect:(CGRect)rect{
    //用黑色绘制已完成的线条
    [[UIColor blackColor] set];
    NSLog(@"%lu",self.finishedLines.count);
    for (BNRLine *line in self.finishedLines) {
        [self strokeLine:line];
    }
    for (BNRLine *line in self.finishedCircles) {
        [self strokeCircle:line];
    }
    
    //用红色绘制正在画的线条
    [[UIColor redColor] set];
    [self strokeLine:self.currentLine];
    [self strokeCircle:self.currentCircle];

    
}
#pragma  mark - touch logical
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if (touches.count == 1) {
        self.currentLine = [[BNRLine alloc] init];
        UITouch *t = [touches anyObject];
        CGPoint location = [t locationInView:self];
        self.currentLine.begin = location;
        self.currentLine.end = location;
    }
    else if(touches.count == 2){
        self.currentCircle = [[BNRLine alloc] init];
        NSMutableSet *mutouches = [touches mutableCopy];
        UITouch *t =  [mutouches anyObject];
        self.currentCircle.begin = [t locationInView:self];
        [mutouches removeObject:t];
        t = [mutouches anyObject];
        self.currentCircle.end = [t locationInView:self];
    }
        
    [self setNeedsDisplay];
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    if (touches.count == 1) {
        UITouch *t = [touches anyObject];
        CGPoint location = [t locationInView:self];
        self.currentLine.end = location;
    }else if (touches.count == 2){
        NSMutableSet *mutouches = [touches mutableCopy];
        UITouch *t =  [mutouches anyObject];
        self.currentCircle.begin = [t locationInView:self];
        [mutouches removeObject:t];
        t = [mutouches anyObject];
        self.currentCircle.end = [t locationInView:self];
        }
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (touches.count == 1) {
        [self.finishedLines addObject:self.currentLine];
        self.currentLine = nil;
        
    }else if (touches.count == 2)
    {
        [self.finishedCircles addObject:self.currentCircle];
        self.currentCircle = nil;
    }
    [self saveData];
    [self setNeedsDisplay];

}
//处理触摸取消事件 如果系统应用被中断 那么触摸事件就会被取消
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    self.currentLine = nil;
    [self saveData];
    [self setNeedsDisplay];
}

//保存至文档中
-(BOOL) saveData{
    
    return [NSKeyedArchiver archiveRootObject:self.finishedLines toFile:self.savePath];
}
//删除
- (BOOL)clearData{
    [self.finishedLines removeAllObjects];
    [self setNeedsDisplay];
    return [NSKeyedArchiver archiveRootObject:nil toFile:self.savePath];
}



@end
