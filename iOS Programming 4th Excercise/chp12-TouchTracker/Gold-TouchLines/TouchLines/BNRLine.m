//
//  BNRLine.m
//  TouchTracker
//
//  Created by lxl on 15/6/15.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRLine.h"

@implementation BNRLine


//SOLUTIONS
-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeCGPoint:[self begin] forKey:@"begin"];
    [aCoder encodeCGPoint:[self end]   forKey:@"end"];
}

//SOLUTIONS
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (self)
    {
        [self setBegin:[aDecoder decodeCGPointForKey:@"begin"]];
        [self setEnd:[aDecoder decodeCGPointForKey:@"end"]];
    }
    
    return self;
}

@end
