//
//  BNRDrawView.h
//  TouchTracker
//
//  Created by lxl on 15/6/15.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNRDrawView : UIView

- (BOOL)saveData;
- (BOOL) clearData;

@end
