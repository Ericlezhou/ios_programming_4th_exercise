//
//  BNRDrawView.m
//  TouchTracker
//
//  Created by lxl on 15/6/15.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRDrawView.h"
#import  "BNRLine.h"

@interface BNRDrawView()

@property (nonatomic,strong) NSMutableDictionary *currentLines;
//支持多点触控，使用一个字典来保存每一个UITouch对象的地址，以及对应的BNRLine对象
/************************
 这时候使用NsValue来将UITouch对象转换为NSValue值，是因为NSDictionary及
 其子类NSMutableDictionary的键必须遵守NSCopying协议，即键必须可以复制，可以相应copy消息，
 UITool并不遵守NSCopying协议，因为每一个触摸事件都是唯一的，不可复制的。
 而NSValue是遵守的，同一个UITouch对象会在触摸过程中创建包含相同内存地址的NSValue对象。
 ************************/
@property (nonatomic,strong) NSMutableArray *finshedLines;
@property (nonatomic,strong) NSString *savePath;//保存历史记录的文件路径
@end

@implementation BNRDrawView

 - (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.finshedLines = [[NSMutableArray alloc] init];
        self.currentLines = [[NSMutableDictionary alloc] init];
        self.backgroundColor = [UIColor grayColor];
        self.multipleTouchEnabled = YES;
        if([[NSKeyedUnarchiver unarchiveObjectWithFile:self.savePath] count] > 0)
        {
            self.finshedLines = [NSKeyedUnarchiver unarchiveObjectWithFile:self.savePath];
        }
    }
    return self;
}


- (NSString*)savePath{
    //获得Documents目录的文件路径
    NSArray *array =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [array firstObject];
    return [path stringByAppendingPathComponent:@"f.archive"];
}


- (void) strokeLine : (BNRLine *)line{
    UIBezierPath *path = [[UIBezierPath alloc]init];
    path.lineWidth = 10;
    path.lineCapStyle = kCGLineCapRound;
    
    [path moveToPoint:line.begin];
    [path addLineToPoint:line.end];
    [path stroke];
}
//重写UIView的drawRect方法

- (void)drawRect:(CGRect)rect{
    //用黑色绘制已完成的线条
    [[UIColor blackColor] set];
    
    for (BNRLine *line in self.finshedLines) {
        [self strokeLine:line];
    }

    
    //用红色绘制正在画的线条
    [[UIColor redColor] set];
    
    for (NSValue *key in self.currentLines.keyEnumerator) {
        [self strokeLine:self.currentLines[key]];
    }
}
#pragma  mark - touch logical
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *t in touches) {
        CGPoint location = [t locationInView:self];
        BNRLine *line = [[BNRLine alloc] init];
        line.begin = location;
        line.end = location;
        NSValue *key = [NSValue valueWithNonretainedObject:t];//This method is useful if you want to add an object to a collection but don’t want the collection to create a strong reference to it.
        self.currentLines[key] = line;
    }
    
    [self setNeedsDisplay];
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *t in touches) {
        NSValue *key = [NSValue valueWithNonretainedObject:t];
        CGPoint point = [t locationInView:self];
        BNRLine *line =  self.currentLines[key];
        line.end = point;
    }
    
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *t in touches) {
        NSValue *key = [NSValue valueWithNonretainedObject:t];
        [self.finshedLines addObject: self.currentLines[key]];
        [self.currentLines removeObjectForKey:key];
    }
    [self setNeedsDisplay];

}
//处理触摸取消事件 如果系统应用被中断 那么触摸事件就会被取消
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *t in touches) {
        NSValue *key = [NSValue valueWithNonretainedObject:t];
        [self.currentLines removeObjectForKey:key];
    }
    [self saveData];
    [self setNeedsDisplay];
}

//保存至文档中
-(BOOL) saveData{
    
    return [NSKeyedArchiver archiveRootObject:self.finshedLines toFile:self.savePath];
}
//删除
- (BOOL)clearData{
    [self.finshedLines removeAllObjects];
    [self setNeedsDisplay];
    return [NSKeyedArchiver archiveRootObject:nil toFile:self.savePath];
}



@end
