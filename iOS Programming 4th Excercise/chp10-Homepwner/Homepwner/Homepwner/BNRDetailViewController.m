//
//  BNRDetailViewController.m
//  Homepwner
//
//  Created by lxl on 15/6/14.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRDetailViewController.h"
#import "BNRItem.h"
#import "BNRDatePickerViewController.h"


@interface BNRDetailViewController ()
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *serialNumberField;
@property (weak, nonatomic) IBOutlet UITextField *valueField;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end

@implementation BNRDetailViewController

-(BNRItem *)itemToShow
{
    self.navigationItem.title = _itemToShow.itemName;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(saveData)];
    return _itemToShow;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"%@",self.itemToShow.description);
    self.nameField.text = self.itemToShow.itemName;
    self.serialNumberField.text = self.itemToShow.serialName;
    self.valueField.text = [NSString stringWithFormat:@"%d",self.itemToShow.valueInDollars];
    self.valueField.keyboardType =  UIKeyboardTypeNumberPad;
    //处理nsdate对象转换为一定风格的string表示
    static NSDateFormatter *formatter = nil;
    if(!formatter){
        formatter = [[NSDateFormatter alloc]init];
        formatter.dateStyle = NSDateFormatterMediumStyle;
        formatter.timeStyle = NSDateFormatterNoStyle;
    }
    self.dateLabel.text = [formatter stringFromDate:self.itemToShow.dateCreated];
    
    
}

-(void) saveData
{
    //取消当前的第一响应对象
    [self.view endEditing:YES];
    self.itemToShow.itemName = self.nameField.text;
    self.itemToShow.serialName = self.serialNumberField.text;
    self.itemToShow.valueInDollars = [self.valueField.text intValue];//强制将字符串转换为整型数据
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self saveData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) changeDate:(id)sender
{
    BNRDatePickerViewController *datePickerController = [[BNRDatePickerViewController alloc] init];
    datePickerController.itemToChangeDate = self.itemToShow;
    [self.navigationController pushViewController:datePickerController animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
