//
//  BNRDatePickerViewController.h
//  Homepwner
//
//  Created by lxl on 15/6/14.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BNRItem;

@interface BNRDatePickerViewController : UIViewController
@property (nonatomic,strong) BNRItem *itemToChangeDate;
@end
