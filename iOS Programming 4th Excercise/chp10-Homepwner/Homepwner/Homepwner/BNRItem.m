//
//  BNRItem.m
//  RandomItems
//
//  Created by lxl on 15/6/7.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRItem.h"

@implementation BNRItem

+ (instancetype) randomItem
{
    NSArray *randomChooseNames = @[@"Fiffy",@"Spork",@"Giga",@"Kuda"];
    NSString *randomName = randomChooseNames[arc4random()%[randomChooseNames count]];
    int randomValue = arc4random()%100;
    NSString *randomNumber = [NSString stringWithFormat:@"%c%c%c%c%c", '0'+arc4random()%10,'A'+arc4random()%26,'0'+arc4random()%10,'A'+arc4random()%26,'0'+arc4random()%10];
    BNRItem *randomItem = [[BNRItem alloc] initWithItemName:randomName valueInDollars:randomValue serialNumber:randomNumber];
    return randomItem;
}

- (instancetype) initWithItemName: (NSString *)name
                  valueInDollars: (int)value
                    serialNumber: (NSString *)sNumber
{
    self = [super init];
    if(self){
        _itemName = name;
        _valueInDollars = value;
        _serialName = sNumber;
        _dateCreated = (NSDate *)[[NSDate alloc] init];
    }
    return self;
}

- (instancetype) initWithItemName:(NSString *)name
{
    return [self initWithItemName:name valueInDollars:0 serialNumber:@"0000"];
}

- (instancetype) initWithItemName:(NSString *)name serialNumber:(NSString *)sNumber
{
    return [self initWithItemName:name valueInDollars:0 serialNumber:sNumber];
}


- (NSString *)description
{
    return [NSString stringWithFormat:@"%@, %@, %d, %@ ", self.itemName, self.serialName, self.valueInDollars, self.dateCreated];
}
@end
