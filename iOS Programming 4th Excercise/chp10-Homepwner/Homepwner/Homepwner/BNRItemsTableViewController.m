//
//  BNRItemsTableViewController.m
//  Homepwner
//
//  Created by lxl on 15/6/12.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//


#import "BNRItemsTableViewController.h"
#import "BNRItem.h"
#import "BNRItemStore.h"
#import "BNRDetailViewController.h"

@interface BNRItemsTableViewController()
@property (nonatomic, strong) IBOutlet UIView *headView;
@end

@implementation BNRItemsTableViewController

- (instancetype)init
{
    self = [super initWithStyle:UITableViewStylePlain];
    //为导航栏设置标题
    self.navigationItem.title = @"Homepwner";
    //为导航栏右侧按钮设置响应，这里使用了UIBarButtonItem，它不是UIView的子类，主要封装了一些设置信息，以便NavigationBar和UIToolBar运行时可以正确的显示该对象。
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewItem:)];
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    return self;
}

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    return [self init];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[BNRItemStore shareStore] allItems] count]+1;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    //添加no more item的一个cell
    if (indexPath.row==[[[BNRItemStore shareStore] allItems] count]) {
        cell.textLabel.text = @"No more items!";
        cell.textLabel.textAlignment = NSTextAlignmentCenter ;
        return cell;
    }
    cell.textLabel.text = [[[[BNRItemStore shareStore] allItems] objectAtIndex:indexPath.row] description];
    return cell;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class]
           forCellReuseIdentifier:@"UITableViewCell"];
    
//    self.tableView.tableHeaderView = self.headView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}


-(UIView *)headView
{
    if (!_headView) {
        [[NSBundle mainBundle] loadNibNamed:@"HeadView" owner:self options:nil];
    }
    return _headView;
}

- (IBAction)toggleEditMode:(UIButton *)sender {
    if(!self.editing){
        [sender setTitle:@"Done" forState:UIControlStateNormal];
        self.editing = YES;
    }
    else{
        [sender setTitle:@"Edit" forState:UIControlStateNormal];
        self.editing = NO;
    }
}

- (IBAction)addNewItem:(UIButton *)sender {
    BNRItem *newItem = [[BNRItemStore shareStore] createItem];
    NSUInteger index = [[[BNRItemStore shareStore] allItems] indexOfObject:newItem];
    // 为目标row定制一个indexpath
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    //插入后来添加的row，并添加动画效果
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
}

//删除一个cell，需要实现如下方法，这里需要处理数据源和cell同步的逻辑
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==[[[BNRItemStore shareStore] allItems] count]) {
        return;
    }
    //根据删除项的indexpath获得对应的item
    BNRItem *item = [[[BNRItemStore shareStore] allItems] objectAtIndex:indexPath.row];
    //将对应的item从数据源中删除，并添加动画效果
    [[BNRItemStore shareStore] removeItem:item];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}
//修改删除确认按钮的标题-bronze excercise
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Remove";//default is “Delete”
}
//配置编辑模式下cell显示的编辑状态，有三种可选择：UITableViewCellEditingStyleNone（不变化）,UITableViewCellEditingStyleDelete（删除）,UITableViewCellEditingStyleInsert（添加）
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==[[[BNRItemStore shareStore] allItems] count]) {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;
}

//相应编辑状态下的移动cell的方法，需要在此处处理数据源同步
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    [[BNRItemStore shareStore] moveItemAtIndex:sourceIndexPath.row toIndex:destinationIndexPath.row];
}
//配置是否允许一个cell可以在编辑模式下可以移动-silver excercisse
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==[[[BNRItemStore shareStore] allItems] count]) {
        return NO;
    }
    return YES;
}
//在编辑模式下，移动一个cell，如果目标index不推荐使用的情况下，可以使用下面的方法配置一个指定的目标cell的indexPath-gold excercisse
-(NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    if (proposedDestinationIndexPath.row==[[[BNRItemStore shareStore] allItems] count]) {
        return sourceIndexPath;
    }
    return proposedDestinationIndexPath;
    
}

//响应点击一个cell的点击事件
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==[[[BNRItemStore shareStore] allItems] count]) {
        return ;
    }
    BNRDetailViewController *detailView = [[BNRDetailViewController alloc] init];
    BNRItem *item = [[[BNRItemStore shareStore] allItems] objectAtIndex:indexPath.row];
    detailView.itemToShow = item;
    [self.navigationController pushViewController:detailView animated:YES];
}




@end













