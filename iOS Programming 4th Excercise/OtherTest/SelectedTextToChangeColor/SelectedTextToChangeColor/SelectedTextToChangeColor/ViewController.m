//
//  ViewController.m
//  SelectedTextToChangeColor
//
//  Created by lxl on 15/6/5.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *headline;
@property (weak, nonatomic) IBOutlet UITextView *body;

@end

@implementation ViewController

- (IBAction)selectedTextColorToChangeButton:(UIButton *)sender {
    [self.body.textStorage addAttribute:NSForegroundColorAttributeName
                                  value:sender.backgroundColor
                                  range:self.body.selectedRange];
    NSLog(@"%@",sender.backgroundColor);
}

- (IBAction)outlineTheSelectedText {
    [self.body.textStorage addAttributes:@{NSStrokeColorAttributeName:[UIColor blackColor],NSStrokeWidthAttributeName:@-1} range:self.body.selectedRange];
}

- (IBAction)unOutlineTheSelectedText {
    [self.body.textStorage removeAttribute:NSStrokeWidthAttributeName range:self.body.selectedRange];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
