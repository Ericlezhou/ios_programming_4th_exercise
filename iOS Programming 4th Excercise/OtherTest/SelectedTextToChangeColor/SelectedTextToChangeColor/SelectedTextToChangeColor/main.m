//
//  main.m
//  SelectedTextToChangeColor
//
//  Created by lxl on 15/6/5.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
