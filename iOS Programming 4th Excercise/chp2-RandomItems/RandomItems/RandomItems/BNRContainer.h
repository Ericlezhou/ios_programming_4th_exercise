//
//  BNRContainer.h
//  RandomItems
//
//  Created by lxl on 15/6/8.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRItem.h"
@interface BNRContainer : BNRItem
@property (nonatomic, copy) NSMutableArray *subitems;
@end
