//
//  main.m
//  RandomItems
//
//  Created by lxl on 15/6/7.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BNRItem.h"
#import "BNRContainer.h"
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSMutableArray *items = [[NSMutableArray alloc] init];
        for (int i =0; i < 3; ++i) {
            [items addObject:[BNRItem randomItem]];
        }
        for (int i = 0; i < 3; i++) {
            NSLog(@"%@",items[i]);
        }
        
        BNRContainer *itemWithContainer = [[BNRContainer alloc] initWithItemName:@"eric pack" valueInDollars:30 serialNumber:@"0A0A0"];
        itemWithContainer.subitems = items;
        NSLog(@"%@", itemWithContainer);
        
        [items addObject:itemWithContainer];
        
        BNRContainer *itemWithContainer2 = [[BNRContainer alloc] initWithItemName:@"eric pack2" valueInDollars:20 serialNumber:@"0A1A0"];
        itemWithContainer2.subitems = items;
        NSLog(@"%@", itemWithContainer2);
        
        itemWithContainer2 = nil;
        itemWithContainer = nil;
        items = nil;
        
    }
    return 0;
}
