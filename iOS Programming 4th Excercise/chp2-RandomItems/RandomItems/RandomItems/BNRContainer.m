//
//  BNRContainer.m
//  RandomItems
//
//  Created by lxl on 15/6/8.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRContainer.h"

@implementation BNRContainer

@synthesize subitems = _subitems;
- (NSMutableArray *)subitems
{
    return _subitems;
}

- (void) setSubitems:(NSMutableArray *)subitems
{
    _subitems = subitems;
}


- (NSString *)description
{
    int value = self.valueInDollars;
    NSMutableString *subitemStr = [[NSMutableString alloc] init];
    for (BNRItem *item in self.subitems) {
        value += item.valueInDollars;
        [subitemStr appendString:item.itemName];
        [subitemStr appendString:@"+"];
    }
    return [NSString stringWithFormat:@"name:%@, value in sum:%d, subitems' name:%@ ", self.itemName, value, subitemStr];
}
@end
