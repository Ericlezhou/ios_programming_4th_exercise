//
//  BNRItemsTableViewController.m
//  Homepwner
//
//  Created by lxl on 15/6/12.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRItemsTableViewController.h"
#import "BNRItem.h"
#import "BNRItemStore.h"


@implementation BNRItemsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class]
           forCellReuseIdentifier:@"UITableViewCell"];
}

- (instancetype)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if(self){
        for (int i = 0; i < 15; i++) {
            [[BNRItemStore shareStore] createItem];
        }
    }
    return self;
}


- (instancetype)initWithStyle:(UITableViewStyle)style
{
    return [self init];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[BNRItemStore shareStore] allItems] count]+1;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    if (indexPath.row==[[[BNRItemStore shareStore] allItems] count]) {
        cell.textLabel.text = @"No more items!";
        return cell;
    }
    cell.textLabel.text = [[[[BNRItemStore shareStore] allItems] objectAtIndex:indexPath.row] description];
    return cell;
    
    
}



@end
