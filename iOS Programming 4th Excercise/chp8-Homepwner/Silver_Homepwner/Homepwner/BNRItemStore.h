//
//  BNRItemStore.h
//  Homepwner
//
//  Created by lxl on 15/6/12.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import <Foundation/Foundation.h>


//使用@class表示该文件只需要使用BNRItem类的声明，无须知道具体的实现细节，这样做的好处是，当该文件引入的头文件改变的话，对于那些使用了@class标注的文件，编译器可以不需要重新编译，今儿节省了编译时间。
@class BNRItem;

@interface BNRItemStore : NSObject
//对于外部来看这里的allItem数组是一个不可变的的数组
@property (nonatomic,readonly) NSArray *allItems;

+ (instancetype)shareStore;
- (BNRItem *)createItem;

@end
