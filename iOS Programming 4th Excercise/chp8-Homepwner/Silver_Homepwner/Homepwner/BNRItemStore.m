//
//  BNRItemStore.m
//  Homepwner
//
//  Created by lxl on 15/6/12.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRItemStore.h"
#import "BNRItem.h"
@interface BNRItemStore()

@property (nonatomic, strong) NSMutableArray *privateItems;

@end


@implementation BNRItemStore



//类方法 实现返回一个单例的BNRItemStore
+(instancetype)shareStore
{
    static BNRItemStore *shareStore = nil;
    if(!shareStore){
        shareStore = [[BNRItemStore alloc] initPrivate];
    }
    return shareStore;
}
//如果调用默认的init方法，就会抛出异常,告诉调用方使用shareStore来初始化一个BNRItemStore
- (instancetype)init
{
    @throw [NSException exceptionWithName:@"Singleton" reason:@"Use +[BNRItemStore shareStore]" userInfo:nil];
    return nil;
}

//私有init方法，只在第一次初始化时被调用
- (instancetype) initPrivate
{
    self = [super init];
    if (self){
        _privateItems = [[NSMutableArray alloc]  init];
    }
    return self;
}

//此处遵循头文件的规定，返回一个不可变的数组，需要对内部的可变数组进行一个copy，得到可变数组的一个不可变副本，还有mutableCopy方法，它是返回相应的可变副本
-(NSArray *)allItems
{
    return [_privateItems copy];
}


-(BNRItem *)createItem
{
    BNRItem *item = [BNRItem randomItem];
    [self.privateItems addObject:item];
    return item;
}


@end

