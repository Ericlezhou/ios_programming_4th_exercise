//
//  BNRItemsTableViewController.m
//  Homepwner
//
//  Created by lxl on 15/6/12.
//  Copyright (c) 2015年 cn.eric. All rights reserved.
//

#import "BNRItemsTableViewController.h"
#import "BNRItem.h"
#import "BNRItemStore.h"

@interface BNRItemsTableViewController() <UITableViewDelegate>

@end

@implementation BNRItemsTableViewController

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row!=[[[BNRItemStore shareStore] allItems]count]) {
        return  60;
        
    }
    return 44;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class]
           forCellReuseIdentifier:@"UITableViewCell"];
    //为tableview设置背景图片
    UIImage *bg = [UIImage imageNamed:@"blue.png"];
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor colorWithPatternImage:bg];
    self.tableView.backgroundView = view;
}

- (instancetype)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if(self){
        for (int i = 0; i < 15; i++) {
            [[BNRItemStore shareStore] createItem];
        }
    }
    return self;
}


- (instancetype)initWithStyle:(UITableViewStyle)style
{
    return [self init];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[BNRItemStore shareStore] allItems] count]+1;
}




-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    if (indexPath.row==[[[BNRItemStore shareStore] allItems] count]) {
        
        cell.textLabel.text = @"No more items!";
        return cell;
    }
    //设置tableviewcell的背景色为透明
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [[[[BNRItemStore shareStore] allItems] objectAtIndex:indexPath.row] description];
    return cell;
    
    
}



@end
