#import "BNRItemsTableViewController.h"
#import "BNRItem.h"
#import "BNRItemStore.h"


@implementation BNRItemsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"UITableViewCell"];
}

- (instancetype)init
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if(self){
        for (int i = 0; i < 5; i++) {
            [[BNRItemStore shareStore] createItem];
        }
    }
    return self;
}

- (instancetype)initWithStyle:(UITableViewStyle)style
{
    return [self init];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [[[BNRItemStore shareStore] largeItems] count];
    }
    else{
        return [[[BNRItemStore shareStore] smallItems] count];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"More than $50";
    }
    else {
        return @"Less than $50";
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell" forIndexPath:indexPath];
    if(indexPath.section == 0){
        cell.textLabel.text = [[[[BNRItemStore shareStore] largeItems] objectAtIndex:indexPath.row]description];
    }
    else{
        cell.textLabel.text = [[[[BNRItemStore shareStore] smallItems] objectAtIndex:indexPath.row]description];
    }
    return cell;
}

@end
